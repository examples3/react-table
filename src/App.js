import React, {useState, useEffect} from 'react';
import './App.css';

import Table from './components/Table/Table';
import UserForm from './components/UserForm/UserForm';

function App() {

    const tableHeaders = {
        name: {label: "Jméno"},
        age: {label: "Věk"},
        sex: {label: "Pohlaví"},
        updatedDate: {label: "Čas aktualizace"}
    };

    const directions = {
        name: 'asc',
        age: 'asc',
        sex: 'asc',
        updatedDate: 'asc'
    };

    const initStateUser = {
        name: '',
        age: 0,
        sex: '',
        updatedDate: ''
    };

    const [dataSource, setDataSource] = useState([]);
    const [selectedUserId, setSelectedUserId] = useState(null);
    const [tableDirections, setTableDirections] = useState(directions);
    const [user, setUser] = useState(initStateUser);

    useEffect(() => {
        loadAsyncData();
    }, []);

    const loadAsyncData = () => new Promise((resolve, reject) => {
        setTimeout(() => resolve(
            setDataSource(require('./store/data.json'))
        ), 1000)
    });

    const setDirectionsHandler = (directions) => {
        setTableDirections(directions);
        setUser(initStateUser);
    };

    const setSelectedUserHandler = (id) => {
        setSelectedUserId(id);
        setUser(dataSource[id]);
    };

    const setUserHandler = (key, val) => {
        const data = {...user};
        data[key] = val;
        setUser(data);
    };

    const saveUserHandled = (user) => {
        const data = [...dataSource];

        if(selectedUserId) {
            data[selectedUserId] = user;
        } else {
            data.push(user);
        }

        setSelectedUserId(null);
        setUser(initStateUser);
        setDataSource(data);
    };

  return (
      <div className="App">
          {dataSource.length < 1 ? (<div>Loading data </div>)  :
          (<React.Fragment>
              <Table
                  dataSource={dataSource}
                  setDataSource={setDataSource}
                  tableSorting={tableDirections}
                  setTableSorting={setDirectionsHandler}
                  tableHeaders={tableHeaders}
                  onSelected={setSelectedUserHandler}
              />
              <UserForm
                  user={user}
                  onSetUser={setUserHandler}
                  onSaveUser={saveUserHandled}
              />
          </React.Fragment>)
          }
      </div>
  );
}

export default App;
