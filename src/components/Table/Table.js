import React from 'react';

import classes from './Table.css';
import Header from "./Header/Header";
import Row from "./Row/Row";

const Table = (props) => {

    const dataSource = props.dataSource;
    const tableHeaders = props.tableHeaders;
    const tableSorting = props.tableSorting;

    const onShortHandler = (key) => {

        const data = [...dataSource];
        const direction = tableSorting[key];

        data.sort((a, b) => {
            return direction === 'asc'
                ? (a[key] > b[key] ? -1 : a[key] < b[key] ? 1 : 0)
                : (b[key] > a[key] ? -1 : b[key] < a[key] ? 1 : 0);
        });

        const directions = {...tableSorting};
        directions[key] = direction === 'asc' ? 'desc' : 'asc';
        props.setDataSource(data);
        props.setTableSorting(directions);
    };

    const headers = <Header data={tableHeaders} onSort={onShortHandler}/>;
    const rows = dataSource.map((item, key) => <Row key={"row" + key} rowNum={key} onSelected={props.onSelected} data={item}/>);

    return (
        <table className={classes.Table}>
            <thead>
                {headers}
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>
    );
};

export default Table;