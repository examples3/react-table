import React from "react";

import classes from "../Table.css";

const Header = (props) => {

    const headers = [];
    for (let id in props.data) {
        headers.push((
            <th
                key={id}
                className={classes.Table__header}
                onClick={() => props.onSort(id)}
            >
                {props.data[id].label}
            </th>
        ));
    }

    headers.push(<th key={"header_edit"} className={classes.Table__header}></th>);
    return (<tr>{headers}</tr>);
};

export default Header;