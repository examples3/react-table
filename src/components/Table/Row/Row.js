import React from "react";

import classes from "../Table.css";

const Row = (props) => {

    const key = props.rowNum;

    let i = 0;
    const cells = [];
    for (let id in props.data) {
        cells.push(<td key={"cell_" +i + key} className={classes.Table__cell}>{props.data[id]}</td>);
        i++;
    }

    cells.push(<td key={"cell_edit" + i + key} className={classes.Table__cell}>
        <button onClick={() => props.onSelected(key)}>Editovat</button>
    </td>);

    return (<tr className={classes.Table__row}>{cells}</tr>);
};

export default Row;