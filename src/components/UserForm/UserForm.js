import React from 'react';

const UserForm = (props) => {

    const user = props.user;
    let errorMessage = null;

    const handleSubmit = (event) => {
        event.preventDefault();

        user.name = user.name.trim();
        user.age = parseInt(user.age);
        user.sex = user.sex.trim();
        user.updatedDate = (new Date()).toISOString();

        const isValid = validate();
        if(false === isValid) {
            alert(errorMessage);
            return;
        }

        props.onSaveUser(user);
    };

    const validate = () => {

        errorMessage = null;

        const onlyLetterPattern = /^[a-zA-Z]+$/;
        const onlyNumberPattern = /^\d+$/;

        if(user.name.length === 0) {
            errorMessage = 'Zadejte jméno!';
            return false;
        }

        if(user.age.length === 0) {
            errorMessage = 'Zadejte věk!';
            return false;
        }

        if(user.sex.length === 0) {
            errorMessage = 'Vyplňte pohlaví!';
            return false;
        }

        if(!onlyLetterPattern.test( user.name )) {
            errorMessage = 'Jméno musí obsahovat pouze písmena!';
            return false;
        }

        if(user.name.length < 2 || user.name.length > 10) {
            errorMessage = 'Délka jména musí být v rozsahu mezi 2 až 10 znaků!';
            return false;
        }

        if(!onlyNumberPattern.test(user.age)) {
            errorMessage = 'Věk musí být číslice!';
            return false;
        }

        if(user.age < 0 || user.age > 129) {
            errorMessage = 'Věk musí být v rozmezí 0 až 129 let!';
            return false;
        }

        if(!['M', 'F', 'O'].includes(user.sex)) {
            errorMessage = `Pole pohlaví obsahuje nepovolený znak "${user.sex}"!`   ;
            return false;
        }

        return true;
    };

    const sexOptions = () => {
        const optionsData = {
            "-" : "--- pohlaví ---",
            "M" : "Muž",
            "F" : "Žena",
            "O" : "Jiné",
        };

        const list = [];
        for (let id in optionsData) {
            const selected = id === user.sex ? "selected" : null;
            list.push(<option key={id} value={id} selected={selected}>{optionsData[id]}</option>);
        }

        return (
            <select name="sex" onChange={(e) => props.onSetUser('sex', e.target.value)}>
                {list}
            </select>
        );
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input key="name" type="text" name="name" value={user.name} onChange={(e) =>props.onSetUser('name', e.target.value)} />
                <input key="age" type="number" name="age" value={user.age} onChange={(e) => props.onSetUser('age', e.target.value)} />
                {sexOptions()}
                <button onClick={handleSubmit}>Save</button>
            </form>
        </div>
    );
};

export default UserForm;